﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        const int port = 8888;
        const string address = "127.0.0.1";
        static NetworkStream stream;
        public static string userName;

        static public void SendtoServer()
        {
            while (true)
            {
                try
                {
                    //формирование числа
                    double RNumber = Math.Clamp(Math.Round(2.0 + new Random().NextDouble(), 1), 2.0, 3.0);
                    Console.WriteLine(userName + ": " + RNumber);
                    //записываем число
                    string message = RNumber.ToString();
                    // преобразуем число в массив байтов
                    byte[] data = Encoding.Unicode.GetBytes(message);
                    // отправка сообщения
                    stream.Write(data, 0, data.Length);
                    // ожидание 2,7с
                    Thread.Sleep(2700);
                }
                catch
                {
                    Console.WriteLine("Ошибка формирования сообщения");
                    break;
                }
            }
        }

            static void Main(string[] args)
        {
            Console.Write("Введите имя клиента:");
            userName = Console.ReadLine();
            TcpClient client = new TcpClient(address, port);
            stream = client.GetStream();

            try
            {


                new Thread(new ThreadStart(SendtoServer)).Start();


                while (true)
                    {

                        var data = new byte[256];
                        StringBuilder Strbuilder = new StringBuilder();
                        int bytes = 0;
                        do
                        {
                            //чтение данных от сервера
                            bytes = stream.Read(data, 0, data.Length);
                            Strbuilder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                        }
                        while (stream.DataAvailable);

                        var message = Strbuilder.ToString();
                        Console.WriteLine("Сервер: {0}", message);
                    }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                client.Close();
            }
        }
    }
        
}

